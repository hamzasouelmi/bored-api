import React from 'react';
import { Spinner } from '@chakra-ui/react'
let Loading = () => {
  return (
    <Spinner
  thickness='4px'
  speed='0.65s'
  emptyColor='gray.200'
  color='red.500'
  size='xl'
/>
  );
};

export default Loading;
