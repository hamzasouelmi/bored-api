import React,{useState} from 'react'
import { Select,stack ,Button, VStack,HStack,Heading,Text} from '@chakra-ui/react'
import axios from 'axios';


function Buttons() {
  
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState("");
const listType = ["education", "recreational", "social", "diy", "charity", "cooking", "relaxation", "music", "busywork"]
const [accessibility, setAccessibility] = useState(0.1);
let [type, setType] = useState("relaxation");
const [participants, setParticipants] = useState(1);
const [price, setPrice] = useState(0.15);
const url = `https://www.boredapi.com/api/activity?type=${type}&aaccessibility=${accessibility}&participants=${participants}&price=${price}`;

let getType = () => {
    setData('');
    axios
      .get(url) 
      .then(res => {
        setData(res.data);
        console.log(res.data);
      })
      .catch(err => console.log(err));
    
  };

  return (
    <>
    <HStack>

     <Select placeholder='nombre de participant'onChange={(e) => setParticipants(e.target.value)}>
              <option value={1}>1</option>
              <option value={2}> 2</option>
              <option value={3}> 3</option>
              <option value={4}> 4</option>
              <option value={5}>5</option>
              <option value={6}> 6</option>
     </Select>

    <Select placeholder='Accessibility :'  onChange={(e) => setAccessibility(e.target.value/10)}>
<option value={0}>0</option>
  <option value={1}>1</option>
  <option value={2}> 2</option>
  <option value={3}> 3</option>
  <option value={4}> 4</option>
  <option value={5}>5</option>
  <option value={6}> 6</option>
</Select>

<Select placeholder='prix'onChange={(e) => setPrice(e.target.value/10)}>
  <option value={0}>FREE</option>
  <option value={1}> Medim </option>
  <option value={2}> high</option>
  <option value={3}>expensive </option>
 
</Select>
<Select placeholder='type' onChange={(e) => setType(e.target.value)}>
{listType.map((type,index)=>
  <option key={index} value={type}>{type}</option>)}
  
</Select>

</HStack>
<Button onClick={getType}color={'green.300'}>Submit Choice</Button>

{data.activity ? (
          <>
          <Heading as='h2' size='2xl'color={'green.800'} fontWeight="extrabold">
          {data.activity}
  
          
          </Heading>
          
          </>
        ) : (
          <center>
    <Text
  bgGradient="linear(to-l, #7928CA, #FF0080)"
  bgClip="text"
  fontSize="6xl"
  fontWeight="extrabold"
>
no activity found 
</Text>
          
          </center>
        )}
    </>
  )
}


export default Buttons
