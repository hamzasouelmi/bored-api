import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Select,Stack ,VStack,Button,Heading,StackDivider, Container} from '@chakra-ui/react'

import Loading from './loading';
import Buttons from './components/Buttons';
export default function App() {
  let [data, setData] = useState('Learn origami');

  let getData = () => {
    setData('');
    axios
      .get('https://www.boredapi.com/api/activity')
      .then(res => {
        setData(res.data);
        console.log(res.data);
      })
      .catch(err => console.log(err));
      
  };

  useEffect(() => {
    getData();
  }, []);
  return (
    <Stack>
      <Container maxW={'md'} bg='gray.200' centerContent>
     <Heading size='lg' fontSize='50px' color={'blue.400'}>
               Getting Bored ??
     </Heading>
     </Container>
      <VStack  

      divider={<StackDivider borderColor='red.200' />}
      spacing={4}
      align='stretch'>

        {data.activity ? (
          <>
          <Heading as='h2' size='2xl'color={'green.800'} fontWeight="extrabold">
          {data.activity}
  
          </Heading>
          <Heading as='h3' size='lg'>
               {data.type}
              </Heading>
           <Heading as='h4' size='md'>
                {data.price}
          </Heading>
          
          </>
        ) : (
          <center>
            <Loading />
          </center>
        )}
         <Button onClick={getData} color={'blue.500'} bg={'pink.200'}>Random Activity</Button>
      </VStack>
      <Buttons/>
     

    </Stack>
  );
}
